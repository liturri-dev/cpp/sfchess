#pragma once
#include "pieces.h"
#include <SFML/Graphics.hpp>
#include <array>
#include <memory>

class Game
{
public:
    Game(sf::RenderWindow &_window);
    void renderBoard();
    void renderPieces();
    void Resize();
    void PrevTexture() { sprites.PrevTexture(); };
    void NextTexture() { sprites.NextTexture(); };
    void SetStartCell(int x, int y);
    void ShowMovs(int x, int y);
    bool MoveTo(int x, int y);
    bool IsSelected() { return board.ValidCoord(startCellX, startCellY); };
    // PiecePos GetStartCell() { return PiecePos(startCellX, startCellY); };

private:
    sf::RenderWindow &window;
    PieceSprites sprites;
    // std::array<std::array<std::shared_ptr<Piece>, 8>, 8> Pieces;
    sf::Texture boardText;

    int startCellX = -1, startCellY = -1;
    float squareSize;
    sf::Vector2f WindowScaleXY;

    // sf::Sprite board;

    // std::vector<std::shared_ptr<Piece>> White;
public:
    Board board;
};