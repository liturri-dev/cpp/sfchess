#include "game.h"
#include <fmt/core.h>

void Game::renderBoard()
{
    // Resize();
    // window.draw(board);
    sf::Color colorW = /*sf::Color::White*/ sf::Color(235, 236, 175, 50);
    sf::Color colorB = /*sf::Color::Black*/ sf::Color(2, 27, 56, 50);
    sf::Color selected = sf::Color(37, 131, 247);
    for (int x = 0; x < 8; x++)
    {
        for (int y = 0; y < 8; y++)
        {
            sf::RectangleShape rec(WindowScaleXY);
            rec.setOrigin(0, 0);
            sf::Vector2f pos{WindowScaleXY.x * x, WindowScaleXY.y * y};
            rec.setPosition(pos);
            if (startCellX == x && startCellY == y && !board.IsEmpty(x, y))
                rec.setFillColor(selected);
            else
                rec.setFillColor(((y + x) % 2 == 1) ? colorB : colorW);
            window.draw(rec);
        }
    }
    if (startCellX >= 0 && startCellX < 8 && startCellY >= 0 && startCellY < 8)
        ShowMovs(startCellX, startCellY);
    renderPieces();
}

void Game::renderPieces()
{
    board.Render();
}

sf::Vector2f operator/(sf::Vector2u vec, float num)
{
    return sf::Vector2f(vec.x / num, vec.y / num);
}

Game::Game(sf::RenderWindow &_window) : window(_window), board(window, sprites)
{
    for (int pawnPos = 0; pawnPos < 8; pawnPos++)
    {
        board.SetPiece<Pawn>(pawnPos, 1, PieceTeam::White);
        board.SetPiece<Pawn>(pawnPos, 6, PieceTeam::Black);
    }
    board.SetPiece<Rook>(0, 0, PieceTeam::White);
    board.SetPiece<Rook>(0, 7, PieceTeam::Black);
    board.SetPiece<Rook>(7, 0, PieceTeam::White);
    board.SetPiece<Rook>(7, 7, PieceTeam::Black);

    board.SetPiece<Knight>(1, 0, PieceTeam::White);
    board.SetPiece<Knight>(1, 7, PieceTeam::Black);
    board.SetPiece<Knight>(6, 0, PieceTeam::White);
    board.SetPiece<Knight>(6, 7, PieceTeam::Black);

    board.SetPiece<Bishop>(2, 0, PieceTeam::White);
    board.SetPiece<Bishop>(2, 7, PieceTeam::Black);
    board.SetPiece<Bishop>(5, 0, PieceTeam::White);
    board.SetPiece<Bishop>(5, 7, PieceTeam::Black);

    board.SetPiece<Queen>(3, 0, PieceTeam::White);
    board.SetPiece<Queen>(3, 7, PieceTeam::Black);
    board.SetPiece<King>(4, 0, PieceTeam::White);
    board.SetPiece<King>(4, 7, PieceTeam::Black);

    // boardText.loadFromFile("../imgs/board.png");
    // board.setTexture(boardText);
};

void Game::SetStartCell(int x, int y)
{
    startCellX = x;
    startCellY = y;
}

void Game::ShowMovs(int x, int y)
{
    if (board.IsEmpty(x, y))
        return;
    auto movs = board.GetValidMoves(x, y);
    sf::Color selected = sf::Color(37, 131, 247, 80);
    for (const auto &pos : movs)
    {
        sf::CircleShape rec(WindowScaleXY.x * 0.4f);
        rec.setOrigin(rec.getGlobalBounds().width / 2, rec.getGlobalBounds().height / 2);
        sf::Vector2f winPos{WindowScaleXY.x * pos.x + WindowScaleXY.x / 2,
                            WindowScaleXY.y * pos.y + WindowScaleXY.y / 2};
        rec.setPosition(winPos);
        rec.setFillColor(selected);
        window.draw(rec);
        // fmt::print("{}x{}\n", pos.x, pos.y);
    }
}
bool Game::MoveTo(int x, int y)
{
    if (!board.ValidCoord(x, y) || !board.ValidCoord(startCellX, startCellY))
        return false;
    auto movs = board.GetValidMoves(startCellX, startCellY);
    if (std::find(movs.begin(), movs.end(), PiecePos(x, y)) == movs.end())
        return false;
    board.Move(startCellX, startCellY, x, y);
    return true;
};

void Game::Resize()
{
    squareSize = std::min((float)window.getSize().x, (float)window.getSize().y);
    WindowScaleXY = {squareSize / 8.0f, squareSize / 8.0f};
};