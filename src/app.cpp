#include "app.h"
#include <fmt/core.h>

MainWindow::MainWindow()
    : window(sf::VideoMode(800, 600), "SFML - Chess", sf::Style::Resize), game(window), currentTurn(PieceTeam::White)
{
    window.setFramerateLimit(60);
    window.setVerticalSyncEnabled(true);
    view = window.getDefaultView();
}

void MainWindow::run()
{
    while (window.isOpen())
    {
        processEvents();
        update();
        render();
    }
}

void MainWindow::processEvents()
{
    sf::Event event;
    while (window.pollEvent(event))
    {
        switch (event.type)
        {
        case sf::Event::Closed:
            window.close();
            break;
        case sf::Event::MouseButtonPressed:
            if (event.mouseButton.button == sf::Mouse::Button::Left)
            {
                auto pos = window.mapPixelToCoords({event.mouseButton.x, event.mouseButton.y}) /
                           (std::min((float)window.getSize().x, (float)window.getSize().y) / 8.0f);
                // int x = pos.x, y = pos.y;

                if (!game.board.ValidCoord(pos.x, pos.y))
                    break;
                if (game.IsSelected())
                {
                    if (game.MoveTo(pos.x, pos.y))
                    {
                        currentTurn = (currentTurn == PieceTeam::White ? PieceTeam::Black : PieceTeam::White);
                        game.SetStartCell(-1, -1);
                    };
                };
                if (game.board.IsTeam(pos.x, pos.y, currentTurn))
                    game.SetStartCell(pos.x, pos.y);
            }
            break;
            // case sf::Event::MouseMoved: {
            //     auto pos = window.mapPixelToCoords({event.mouseMove.x, event.mouseMove.y}) /
            //                (std::min((float)window.getSize().x, (float)window.getSize().y) / 8.0f);
            //     int x = pos.x, y = pos.y;
            //     // fmt::print("x:{} y:{}\n", (int)pos.x, (int)pos.y);
            //     if (x >= 0 && x < 8 && y >= 0 && y < 8)
            //         game.SetStartCell(x, y);
            // }
            break;
        case sf::Event::KeyPressed:
            switch (event.key.code)
            {
            case sf::Keyboard::Key::F2:
                game.PrevTexture();
                break;
            case sf::Keyboard::Key::F3:
                game.NextTexture();
                break;
            case sf::Keyboard::Key::Escape:
                window.close();
                break;
            default:
                break;
            }
            break;
        case sf::Event::Resized: {
            // window.setView(sf::View(sf::FloatRect(0.0f, 0.0f, event.size.width, event.size.height)));
            float squareSize = std::min((float)event.size.width, (float)event.size.height);
            view.setSize((float)event.size.width + 20, (float)event.size.height + 20);
            view.setCenter(squareSize / 2.0f, squareSize / 2.0f);
            window.setView(view);
            WindowScaleXY = {squareSize / 8.0f, squareSize / 8.0f};
            game.Resize();

            break;
        }
        default:
            break;
        }
    }
}

void MainWindow::update()
{
    // Actualizar el estado de la aplicación aquí
    // ...
}

void MainWindow::render()
{
    window.clear(sf::Color(0x8a745400));
    game.renderBoard();
    // Actualizar la ventana
    window.display();
}
