#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/System/Vector2.hpp>
#include "game.h"


class MainWindow
{
public:
    MainWindow();

    // Bucle principal de la aplicación
    void run();

private:
    // Procesar los eventos de la ventana
    void processEvents();

    // Actualizar la lógica de la aplicación
    void update();

    // Dibujar en la ventana



    void render();

    sf::RenderWindow window;
    sf::View view;
    sf::Vector2f WindowScaleXY;
    Game game;
    PieceTeam currentTurn;
};
