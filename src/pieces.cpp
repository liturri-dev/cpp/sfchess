#include "pieces.h"
#include <iostream>
#include <string>

PieceSprites::PieceSprites() : activeTexture(0)
{
    for (std::string item : {"chess_01_deopt.png", "chess_02_deopt.png", "chess_03_deopt.png", "chess_04_deopt.png",
                             "chess_05_deopt.png", "chess_06_deopt.png"})
    {
        sf::Texture text;
        if (!text.loadFromFile("../imgs/" + item))
        {
            std::cout << "No se puede abrir la textura: "
                      << "../imgs/" + item << std::endl;
        }
        else
        {
            text.setSmooth(true);
            textureList.push_back(text);
        }
    };
    activeTexture = activeTexture % textureList.size();
}

sf::Sprite PieceSprites::GetTexture(PieceType type, PieceTeam team)
{
    auto size = textureList[activeTexture].getSize();
    sf::Sprite sprite(textureList[activeTexture]);

    auto teamLine = (team == PieceTeam::White ? 0 : static_cast<int>(size.y / 2));

    switch (type)
    {
    case PieceType::Pawn:
        sprite.setTextureRect({0, teamLine, static_cast<int>(size.x / 6), static_cast<int>(size.y / 2)});
        sprite.setOrigin(size.x / 6.0f / 2, size.y / 2.0f / 2);
        break;
    case PieceType::Rook:
        sprite.setTextureRect(
            {static_cast<int>(size.x / 6), teamLine, static_cast<int>(size.x / 6), static_cast<int>(size.y / 2)});
        sprite.setOrigin(size.x / 6.0f / 2, size.y / 2.0f / 2);
        break;
    case PieceType::Knight:
        sprite.setTextureRect(
            {static_cast<int>(size.x / 6) * 2, teamLine, static_cast<int>(size.x / 6), static_cast<int>(size.y / 2)});
        sprite.setOrigin(size.x / 6.0f / 2, size.y / 2.0f / 2);
        break;
    case PieceType::Bishop:
        sprite.setTextureRect(
            {static_cast<int>(size.x / 6) * 3, teamLine, static_cast<int>(size.x / 6), static_cast<int>(size.y / 2)});
        sprite.setOrigin(size.x / 6.0f / 2, size.y / 2.0f / 2);
        break;
    case PieceType::Queen:
        sprite.setTextureRect(
            {static_cast<int>(size.x / 6) * 4, teamLine, static_cast<int>(size.x / 6), static_cast<int>(size.y / 2)});
        sprite.setOrigin(size.x / 6.0f / 2, size.y / 2.0f / 2);
        break;
    case PieceType::King:
        sprite.setTextureRect(
            {static_cast<int>(size.x / 6) * 5, teamLine, static_cast<int>(size.x / 6), static_cast<int>(size.y / 2)});
        sprite.setOrigin(size.x / 6.0f / 2, size.y / 2.0f / 2);
        break;
    }
    return sprite;
}

sf::Sprite Piece::Render()
{
    auto sprite = sprites.GetTexture(GetType(), team);
    // sprite.setOrigin();
    sprite.setPosition({(float)pos.x, (float)pos.y});
    return sprite;
}

void Board::Render()
{
    float squareSize = std::min((float)window.getSize().x, (float)window.getSize().y) / 8.0f;
    for (const auto &row : Pieces)
        for (const auto &col : row)
        {
            if (col == NULL)
                continue;
            auto sprt = col->Render();
            auto scale = (squareSize * 0.82) / std::max(sprt.getLocalBounds().width, sprt.getLocalBounds().height);
            sprt.setPosition(sprt.getPosition().x * squareSize + squareSize / 2.0f,
                             sprt.getPosition().y * squareSize + squareSize / 2.0f);
            sprt.setScale(scale, scale);
            window.draw(sprt);
        }
}

std::vector<PiecePos> Pawn::GetValidMoves(const Board &board) const
{
    std::vector<PiecePos> movs;
    if (team == PieceTeam::White)
    {
        if (board.ValidCoord(pos.x - 1, pos.y + 1) && board.IsTeam(pos.x - 1, pos.y + 1, PieceTeam::Black))
            movs.push_back({pos.x - 1, pos.y + 1});

        if (board.ValidCoord(pos.x + 1, pos.y + 1) && board.IsTeam(pos.x + 1, pos.y + 1, PieceTeam::Black))
            movs.push_back({pos.x + 1, pos.y + 1});

        if (!board.IsEmpty(pos.x, pos.y + 1))
            return movs;
        movs.push_back({pos.x, pos.y + 1});
        if (pos.y == 1 && board.IsEmpty(pos.x, pos.y + 2))
            movs.push_back({pos.x, pos.y + 2});
    }
    else
    {
        if (board.ValidCoord(pos.x - 1, pos.y - 1) && board.IsTeam(pos.x - 1, pos.y - 1, PieceTeam::White))
            movs.push_back({pos.x - 1, pos.y - 1});

        if (board.ValidCoord(pos.x + 1, pos.y - 1) && board.IsTeam(pos.x + 1, pos.y - 1, PieceTeam::White))
            movs.push_back({pos.x + 1, pos.y - 1});

        if (!board.IsEmpty(pos.x, pos.y - 1))
            return movs;
        movs.push_back({pos.x, pos.y - 1});
        if (pos.y == 6 && board.IsEmpty(pos.x, pos.y - 2))
            movs.push_back({pos.x, pos.y - 2});
    }
    return movs;
};

std::vector<PiecePos> Knight::GetValidMoves(const Board &board) const
{
    std::vector<PiecePos> movs;
    int x = pos.x, y = pos.y;
    PieceTeam OtherTeam = (team == PieceTeam::White ? PieceTeam::Black : PieceTeam::White);
    std::array<std::pair<int, int>, 8> coords{{{x - 1, y - 2},
                                               {x + 1, y - 2},
                                               {x - 2, y - 1},
                                               {x + 2, y - 1},
                                               {x + 2, y + 1},
                                               {x - 2, y + 1},
                                               {x - 1, y + 2},
                                               {x + 1, y + 2}}};
    for (const auto &[x_, y_] : coords)
    {
        if (board.ValidCoord(x_, y_) && (board.IsEmpty(x_, y_) || board.IsTeam(x_, y_, OtherTeam)))
            movs.push_back(PiecePos{x_, y_});
    }
    return movs;
}

std::vector<PiecePos> Rook::GetValidMoves(const Board &board) const
{
    std::vector<PiecePos> movs;
    PieceTeam OtherTeam = (team == PieceTeam::White ? PieceTeam::Black : PieceTeam::White);
    // int x = pos.x, y = pos.y;
    for (int x = pos.x + 1; board.ValidCoord(x, pos.y); x++)
    {
        if (board.IsTeam(x, pos.y, team))
            break;
        movs.push_back({x, pos.y});
        if (board.IsTeam(x, pos.y, OtherTeam))
            break;
    };
    for (int x = pos.x - 1; board.ValidCoord(x, pos.y); x--)
    {
        if (board.IsTeam(x, pos.y, team))
            break;
        movs.push_back({x, pos.y});
        if (board.IsTeam(x, pos.y, OtherTeam))
            break;
    };
    for (int y = pos.y + 1; board.ValidCoord(pos.x, y); y++)
    {
        if (board.IsTeam(pos.x, y, team))
            break;
        movs.push_back({pos.x, y});
        if (board.IsTeam(pos.x, y, OtherTeam))
            break;
    };
    for (int y = pos.y - 1; board.ValidCoord(pos.x, y); y--)
    {
        if (board.IsTeam(pos.x, y, team))
            break;
        movs.push_back({pos.x, y});
        if (board.IsTeam(pos.x, y, OtherTeam))
            break;
    };
    return movs;
}

std::vector<PiecePos> Bishop::GetValidMoves(const Board &board) const
{
    std::vector<PiecePos> movs;
    PieceTeam OtherTeam = (team == PieceTeam::White ? PieceTeam::Black : PieceTeam::White);
    // int x = pos.x, y = pos.y;
    for (int x = pos.x + 1, y = pos.y + 1; board.ValidCoord(x, y); x++, y++)
    {
        if (board.IsTeam(x, y, team))
            break;
        movs.push_back({x, y});
        if (board.IsTeam(x, y, OtherTeam))
            break;
    };
    for (int x = pos.x - 1, y = pos.y + 1; board.ValidCoord(x, y); x--, y++)
    {
        if (board.IsTeam(x, y, team))
            break;
        movs.push_back({x, y});
        if (board.IsTeam(x, y, OtherTeam))
            break;
    };
    for (int x = pos.x + 1, y = pos.y - 1; board.ValidCoord(x, y); x++, y--)
    {
        if (board.IsTeam(x, y, team))
            break;
        movs.push_back({x, y});
        if (board.IsTeam(x, y, OtherTeam))
            break;
    };
    for (int x = pos.x - 1, y = pos.y - 1; board.ValidCoord(x, y); x--, y--)
    {
        if (board.IsTeam(x, y, team))
            break;
        movs.push_back({x, y});
        if (board.IsTeam(x, y, OtherTeam))
            break;
    };
    return movs;
}

std::vector<PiecePos> King::GetValidMoves(const Board &board) const
{
    std::vector<PiecePos> movs;
    PieceTeam OtherTeam = (team == PieceTeam::White ? PieceTeam::Black : PieceTeam::White);
    int x = pos.x, y = pos.y;
    std::array<std::pair<int, int>, 8> coords{{{x - 1, y - 1},
                                               {x, y - 1},
                                               {x + 1, y - 1},
                                               {x - 1, y},
                                               {x + 1, y},
                                               {x - 1, y + 1},
                                               {x, y + 1},
                                               {x + 1, y + 1}}};
    for (const auto &[x_, y_] : coords)
    {
        if (board.ValidCoord(x_, y_) && (board.IsEmpty(x_, y_) || board.IsTeam(x_, y_, OtherTeam)))
            movs.push_back(PiecePos{x_, y_});
    }
    return movs;
}

std::vector<PiecePos> Queen::GetValidMoves(const Board &board) const
{
    std::vector<PiecePos> movs;
    PieceTeam OtherTeam = (team == PieceTeam::White ? PieceTeam::Black : PieceTeam::White);
    // int x = pos.x, y = pos.y;
    for (int x = pos.x + 1, y = pos.y + 1; board.ValidCoord(x, y); x++, y++)
    {
        if (board.IsTeam(x, y, team))
            break;
        movs.push_back({x, y});
        if (board.IsTeam(x, y, OtherTeam))
            break;
    };
    for (int x = pos.x - 1, y = pos.y + 1; board.ValidCoord(x, y); x--, y++)
    {
        if (board.IsTeam(x, y, team))
            break;
        movs.push_back({x, y});
        if (board.IsTeam(x, y, OtherTeam))
            break;
    };
    for (int x = pos.x + 1, y = pos.y - 1; board.ValidCoord(x, y); x++, y--)
    {
        if (board.IsTeam(x, y, team))
            break;
        movs.push_back({x, y});
        if (board.IsTeam(x, y, OtherTeam))
            break;
    };
    for (int x = pos.x - 1, y = pos.y - 1; board.ValidCoord(x, y); x--, y--)
    {
        if (board.IsTeam(x, y, team))
            break;
        movs.push_back({x, y});
        if (board.IsTeam(x, y, OtherTeam))
            break;
    };
    for (int x = pos.x + 1; board.ValidCoord(x, pos.y); x++)
    {
        if (board.IsTeam(x, pos.y, team))
            break;
        movs.push_back({x, pos.y});
        if (board.IsTeam(x, pos.y, OtherTeam))
            break;
    };
    for (int x = pos.x - 1; board.ValidCoord(x, pos.y); x--)
    {
        if (board.IsTeam(x, pos.y, team))
            break;
        movs.push_back({x, pos.y});
        if (board.IsTeam(x, pos.y, OtherTeam))
            break;
    };
    for (int y = pos.y + 1; board.ValidCoord(pos.x, y); y++)
    {
        if (board.IsTeam(pos.x, y, team))
            break;
        movs.push_back({pos.x, y});
        if (board.IsTeam(pos.x, y, OtherTeam))
            break;
    };
    for (int y = pos.y - 1; board.ValidCoord(pos.x, y); y--)
    {
        if (board.IsTeam(pos.x, y, team))
            break;
        movs.push_back({pos.x, y});
        if (board.IsTeam(pos.x, y, OtherTeam))
            break;
    };
    return movs;
}
