#pragma once
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <array>
#include <memory>
#include <random>

using PiecePos = sf::Vector2i;

enum class PieceType
{
    Pawn = 0,
    Rook,
    Knight,
    Bishop,
    Queen,
    King
};

enum class PieceTeam
{
    White,
    Black
};

class PieceSprites
{
public:
    PieceSprites();

    PieceSprites(const PieceSprites &) = delete;
    PieceSprites(PieceSprites &&) = delete;
    PieceSprites &operator=(const PieceSprites &) = delete;
    PieceSprites &operator=(PieceSprites &&) = delete;

    sf::Sprite GetTexture(PieceType type, PieceTeam team);
    void NextTexture() { activeTexture = (activeTexture + 1) % textureList.size(); };
    void PrevTexture() { activeTexture = (activeTexture - 1 + textureList.size()) % textureList.size(); };

private:
    std::vector<sf::Texture> textureList;
    int activeTexture;
};

class Board;

class Piece
{
public:
    Piece(PiecePos pos_, PieceTeam team_, PieceSprites &sprites_) : pos(pos_), team(team_), sprites(sprites_){};
    // Piece(int posH, int posV, PieceSprites &sprites_) : pos(posH, posV), sprites(sprites_){};
    virtual ~Piece() = default;
    sf::Sprite Render();
    virtual PieceType GetType() const = 0;
    void SetPos(int x, int y) { pos = {x, y}; };
    PieceTeam GetTeam() const { return team; };
    virtual std::vector<PiecePos> GetValidMoves([[maybe_unused]] const Board &board) const = 0;

protected:
    PiecePos pos;
    PieceTeam team;
    PieceSprites &sprites;
};

class Pawn : public Piece
{
public:
    using Piece::Piece;
    PieceType GetType() const override { return PieceType::Pawn; };
    std::vector<PiecePos> GetValidMoves(const Board &board) const override;
};

class Rook : public Piece
{
public:
    using Piece::Piece;
    PieceType GetType() const override { return PieceType::Rook; };
    std::vector<PiecePos> GetValidMoves(const Board &board) const override;
};

class Knight : public Piece
{
public:
    using Piece::Piece;
    PieceType GetType() const override { return PieceType::Knight; };
    std::vector<PiecePos> GetValidMoves(const Board &board) const override;
};

class Bishop : public Piece
{
public:
    using Piece::Piece;
    PieceType GetType() const override { return PieceType::Bishop; };
    std::vector<PiecePos> GetValidMoves(const Board &board) const override;
};

class Queen : public Piece
{
public:
    using Piece::Piece;
    PieceType GetType() const override { return PieceType::Queen; };
    std::vector<PiecePos> GetValidMoves(const Board &board) const override;
};

class King : public Piece
{
public:
    using Piece::Piece;
    PieceType GetType() const override { return PieceType::King; };
    std::vector<PiecePos> GetValidMoves(const Board &board) const override;
};

class Board
{
public:
    Board(sf::RenderWindow &window_, PieceSprites &sprites_) : window(window_), sprites(sprites_) /*, gen(rd())*/ {};
    template <typename T>
    void SetPiece(int x, int y, PieceTeam team)
    {
        // std::uniform_int_distribution<int> randPos(0, 7);

        // do
        // {
        //     x = randPos(gen);
        //     y = randPos(gen);
        // } while (!IsEmpty(x, y));
        Pieces[x][y] = std::make_shared<T>(PiecePos{x, y}, team, sprites);
    };
    void Render();

    bool IsEmpty(int x, int y) const { return Pieces[x][y] == nullptr; };
    bool IsTeam(int x, int y, PieceTeam team) const { return !IsEmpty(x, y) && Pieces[x][y]->GetTeam() == team; };
    std::vector<PiecePos> GetValidMoves(int x, int y) const { return Pieces[x][y]->GetValidMoves(*this); };
    void Move(int x1, int y1, int x2, int y2)
    {
        Pieces[x2][y2] = std::move(Pieces[x1][y1]);
        Pieces[x1][y1] = nullptr;
        Pieces[x2][y2]->SetPos(x2, y2);
    };

    bool ValidCoord(int x, int y) const { return x >= 0 && x <= 7 && y >= 0 && y <= 7; };

private:
    std::array<std::array<std::shared_ptr<Piece>, 8>, 8> Pieces;
    sf::RenderWindow &window;
    PieceSprites &sprites;

    // std::random_device rd;
    // std::mt19937 gen;
};